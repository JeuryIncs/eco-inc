<?php

 $idFormulario = array('class' => 'formulario');

echo form_open('welcome/recibirDatatosRegistrar',$idFormulario)
?>
<div class='cuerpo'>	
	
		<a href='#' data-reveal-id='myModal'> <div class='FotoFrase'>
		
					
			<img id='fotografia' class='btnRegistrar' src='img/btnEcoInc.gif'/>
			<div id='myModal' class='reveal-modal'>										

			<a class="close-reveal-modal">&#215;</a>
 	
 		<h1 class='datosPersonales'>Datos personales</h1><br>


	<figure>
		<div class="registroUsuario">
<?php
$nombre = array(
	'name'=>'nombre',
	'class' => 'login-field',
	'type' => 'text',
	'id' => 'nombre',
	'placeholder' => 'Nombre'
	);
$apellido = array(
	'name' => 'apellido',
	'class' => 'login-field',
	'type' =>  'text',
	'placeholder' => 'Apellido',
	'id' => 'apellido'
	);
$sexo =  array(
	'sexo' => 'Sexo',
	'hombre' => 'Hombre',
	'mujer' => 'Mujer',

	);
$telefono = array(
	'name' => 'telefono',
	'class' => 'login-field',
	'id' => 'dateTelefono',
	'type' => 'text',
	'placeholder' => 'Telefono'
	);
$fecha = array(
	'name' => 'fecha',
	'class' => 'login-field',
	'type' => 'date',
	'id' => 'fecha'

	);
$usuario = array(
	'name' => 'usuario',
	'type' => 'text',
	'id' => 'usuario',
	'placeholder' => 'Usuario',
	'class' => 'login-field'
	);
 $email = array(
	'name' => 'email',
	'type' => 'email',
	'id' => 'email',
	'placeholder' => 'Email',
	'class' => 'login-field'
	);
$password = array(
	'name' => 'password',
	'type' => 'password',
	'id' => 'password',
	'placeholder' => 'Password',
	'class' => 'login-field mostrarPassword'
	); 
$ConfirmarPassword = array(
	'name' => 'confirmarPassword',
	'type' => 'password',
	'id' => 'ConfirmarPass',
	'placeholder' => 'Confirmar Password',
	'class' => 'login-field mostrarPassword'
	);   
$registrame = array(
	'name' => 'btnRegistrarse',
	'type' => 'submit',
	'value' => 'Registarme',
	'class' => 'btnRegistarme'
	); 
	  
		    echo form_input($nombre);
		    echo form_input($apellido);		    
		    echo form_dropdown('sexo', $sexo,'','class="login-field" id="idsexos"');
		    echo form_dropdown('nombreProvincia', $provincia ,'', 'class="login-field" id="idprovincias"');
		    echo form_input($telefono);
		    echo form_input($fecha);
?>
		    
	    </div>
			<h1 class='datosUsuarios'>Datos de Usuarios</h1><br>

	    <div class="registroUsuario">
<?php
			echo form_input($usuario);
			echo form_input($email);
			echo form_input($password);
			echo form_input($ConfirmarPassword);
			echo form_input($registrame);
?>
		  
        </div>

<p class='mostrarPass' ><i class="fa fa-eye fa-lg"></i><p>

</figure>
		
		</div></a>
</div>
<br>
<br>
        <div class="btnRegistrarFacebook" onclick="hello('facebook').login()" >

        </div>
	
	
<br>
	    <div class="btnRegistrarTwitter" >

        </div>


<?php 
echo form_close()
?>
	

</div>
