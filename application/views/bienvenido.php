
<div class='escribenos'>

<h1 class='divTitulo'> Escribenos, comunicate con nosotros!</h1>

<div class='contenedorMensaje'>
<?php

echo form_open("index/enviarCorreo");

	
	$nombre = array(
		'name' =>  'nombre',
		'class' => 'txtNombre'
		);	
	$correo = array(
		'name' =>  'correo',
		'class' => 'txtCorreoEscirbenos'
		);
	$titulo = array(
		'name' =>  'titulo',
		'class' => 'txtTitulo'
		);
	$mensaje = array(
	    'name' =>  'mensaje',
		'class' => 'txtNombre',
       	'rows'  => '5',
        'cols'  => '10'
		);
	$btnEnviar = array(
	    'name' => 'btnEnviar',
	    'value' => 'Enviar',
	    'class' => 'btnEnviar',
	    'type' => 'submit'
		);
	$btnCancelar = array(
	    'name' => 'btnCancelar',
	    'value' => 'Cancelar',
	    'class' => 'btnCancelar',
	    'type' => 'button'
	    );
	$classNombre  = array(
		'class' =>  'lblNombre'
		);
	$classCorreo  = array(	
		'class' =>  'lblCorreo'
		);
	$classTitulo  = array(
		'class' =>  'lblTitulo'
		);
	$classMensaje  = array(
		'class' =>  'lblMensaje'
		);

  	echo form_label('*Nombre: ','',$classNombre);
	echo form_input($nombre);
  	echo form_label('*Correo: ','',$classCorreo);
	echo form_input($correo);
  	echo form_label('*Titulo: ','',$classTitulo);
	echo form_input($titulo);
  	echo form_label('*Mensaje: ','',$classMensaje);
	echo form_textarea($mensaje);
	echo form_button('btnCancelar','Cancelar',"class='btnCancelar'");
	echo form_submit($btnEnviar);


echo form_close();



?>
</div>
	<a href='http://www.twitter.com/ecoincs' class='infoMensaje'>Powered by Eco Inc </a>
</div>
<div class='menuInicio'>

<div class='ingresos'>
    <p>Ingresos <i class="fa fa-money fa-1x"></i></p>
</div>


<div class='gastos'>
	<p>Gastos <i class="fa fa-usd  fa-1x"></i></p>
</div>


<div class='ahorro'>
    <p>Ahorros <i class="fa fa-signal fa-1x"></i></p>
</div>

<div class='Tips'>
	<p>Tips <i class="fa fa-comment fa-1x"></i></p>
</div>
</div>

<div class='informacionInicio'>

</div>


<div class='cuadroGastos'>

	<p class='letrasGatos'>Agregar Nuevos Gastos</p>
	<i id='cerrarAgregarGastos' class="fa fa-times-circle fa-2x"></i>
	<hr>

<?php
$buscadorGastos = array(
		'name' =>  'buscadorGastos',		
		'class' => 'txtBuscadorGastos',
		'placeholder' => 'Busca tus gastos guardados'
		);

/*Pregutnar poner el icono aqui*/
$btnAgregarGastos = array(
	    'name' => 'btnAgregarGastos',
	    'value' => 'Agregar',
	    'type' => 'button'
	    );


echo form_button('btnAgregarGastos','Agregar',"class='btnAgregarGastos'" );

echo form_input($buscadorGastos);
?>

<div class="cuadro">  
<p  class='iconoBuscador'><i class="fa fa-search fa-2x"></i><p>
</div>

<div class='tablaGastos'>	
<table id="tablaGastos">
		<thead>
			<tr class='filaArriba'>
				<th scope="col" class="rounded-q2">Monto</th>
				<th scope="col" class="rounded-q3">Descripcion</th>
				<th scope="col" class="rounded-q3">Fecha</th>
				<th scope="col" class="rounded-q4">Acion</th>
			 </tr>
	    </thead>
        <tfoot>
    	<tr>
        	<td colspan="4" class="rounded-foot-left"><em> Powered by Eco Inc</em></td>
        </tr>
    </tfoot>
    <tbody id="tablaGasto">
	
	
    	<?php 

    	foreach ($gastosGuardados as $datos ) {
    		
        echo "<tr>";
		echo "<td>".$datos['monto']."</td>";
		echo "<td>".$datos['descripcion']."</td>";
		echo "<td>".$datos['fecha']."</td>";

		 echo "<td><a>  <p class='modificarGastos'><i class='fa fa-pencil fa-2x'>
		 </i></p></a><a href=\"index/eliminarGastos?id=".$datos['id']."\"><p class='eliminarGasto'> <i class='fa fa-trash-o fa-2x'>
		 </i></p></a></a></td>";

		echo "</tr>";	
    	}

	?>

    </tbody>
</table>	
	</div>


	<?php 

    	foreach ($totalGastos as $totalGastos) {
    		echo "<div class='divTotalGastos'> <p>Su total de gastos acumulado es: </p><div class='divTotalGasto'>
    		<b><p class='fondoTotal'>".($totalGastos['total'])." $</p></b></div></div>";	
    	}	

	 ?>


</div>

<div class='cuadroIngresos'>

	<p class='letrasIngresos'>Agregar Nuevos Ingresos</p>
	<i id='cerrarAgregarIngresos' class="fa fa-times-circle fa-2x"></i>
	<hr>

<?php
$buscador = array(
		'name' =>  'buscadorIngresos',
		'class' => 'txtBuscadorIngresos',
		'placeholder' => 'Busca tus Ingresos guardados'
		);

/*Pregutnar poner el icono aqui*/
$btnAgregarIngresos = array(
	    'name' => 'btnAgregarIngresos',
	    'value' => 'Agregar',
	    'type' => 'button'
	    );


echo form_button('btnAgregarIngresos','Agregar',"class='btnAgregarIngresos'" );

echo form_input($buscador);
?>

<div class="cuadro">  
<p  class='iconoBuscador'><i class="fa fa-search fa-2x"></i><p>
</div>

<div class='tablaIngresos'>	
<table id="tablaIngresos">
		<thead>
			<tr class='filaArriba'>
				<th scope="col" class="rounded-q2">Monto</th>
				<th scope="col" class="rounded-q3">Descripcion</th>
				<th scope="col" class="rounded-q3">Fecha</th>
				<th scope="col" class="rounded-q4">Acion</th>
			 </tr>
	    </thead>
        <tfoot>
    	<tr>
        	<td colspan="4" class="rounded-foot-left"><em> Powered by Eco Inc</em></td>
        </tr>
    </tfoot>
    <tbody id='tablaIngreso'>
	
	
    	<?php 		


  //   	foreach ($ingresosGuardados as $datos) {
    		
  //       echo "<tr>";
		// echo "<td>".$datos['monto']."</td>";
		// echo "<td>".$datos['descripcion']."</td>";
		// echo "<td>".$datos['fecha']."</td>";
		// echo "<td><a><p class='btnModificarIngresos'><i class='fa fa-pencil fa-2x'>
		//  </i></p></a><a href=\"index/eliminarIngresos?id=".$datos['id']."\"><p class='eliminarIngresos'> <i class='fa fa-trash-o fa-2x'>
		//  </i></p></a></a></td>";
		// echo "</tr>";	



  	//}



		
	?>

	<?php foreach ($ingresosGuardados as $datos): ?>
		<tr>
			<td> <?= $datos['monto'] ?></td>
			<td> <?= $datos['descripcion'] ?></td>
			<td> <?= $datos['fecha'] ?></td>
			<td> 
				<a class='btnModificarIngresos'>
					<p class="btnModificarIngresos">
						<i class="fa fa-pencil fa-2x"></i>
					</p>
					<a href="index/eliminarIngresos?id=<?= $datos['id'] ?>">
						<p class='eliminarIngresos'> <i class='fa fa-trash-o fa-2x'></i>
						</p>
					</a>
				</a>
			</td>
		</tr>
	<?php endforeach; ?>

    </tbody>
</table>	
	</div>

	<?php 

    	foreach ($totalIngresosDinero as $totalIngresos) {
    		echo "<div class='divTotalIngresos'> <p>Su total de ingresos acumulado es: </p><div class='divTotalIngreso'> <b><p class='fondoTotal'>".($totalIngresos['total'])." $</p></b></div></div>";	
    	}	
	 ?>
 </div>	

<div class='registrarNuevoIngresos'>


	<p class='letrasNuevoIngresos'>Registrar nuevo ingreso</p>
	<hr>


<?php

$idAgregarIngresos = array('class' => 'agregarIngresos');

echo form_open("index/agregarIngresos",$idAgregarIngresos);

$cantidad = array(
	'name' => 'cantidadIngresos',
	'placeholder' => '¿De cuanto fue tu nuevo ingreso?',
	'class' => 'txtCantidadRegistrar',
	'onkeypress' => 'return justNumbers(event);'
	);
$descripcion = array(
	'name' => 'descripcionIngresos',
	'placeholder' => '¿Como lograste esa cantidad?',
	'class'  => 'descripcionIngresos'
	);

$btnCancelarRegistro = array(
	    'name' => 'btnAgregarIngresos',
	    'value' => 'Cancelar'
	    );
$btnSalvarRegistro = array(
	    'name' => 'btnAgregarIngresos',
   		'class' => 'btnSalvar',
	    'value' => 'Guardar'
	    );

echo form_input($cantidad);
echo form_textarea($descripcion);
echo form_button('btnCancelarRegistro','Cancelar',"class='btnCancelarRegistroIngreso'" );
echo form_submit($btnSalvarRegistro);




echo form_close();

?>


</div>

<div class='registrarNuevoGastos'>


	<p class='letrasNuevoIngresos'>Registrar nuevo gastos</p>
	<hr>
<?php

$idAgregarGastos = array('class' => 'agregarGastos');

echo form_open("index/agregarGastos",$idAgregarGastos);

$cantidad = array(
	'name' => 'cantidadGastos',
	'placeholder' => '¿De cuanto fue tu nuevo Gasto?',
	'class' => 'txtCantidadRegistarGasto',
	'onkeypress' => 'return justNumbers(event);'
	);
$descripcion = array(
	'name' => 'descripcionGastos',
	'placeholder' => '¿Por que gastaste cantidad?',
	'class'  => 'descripcionGastos'
	);

$btnCancelarRegistroGastos = array(
	    'name' => 'btnAgregarGastos',
	    'value' => 'Cancelar'
	    );
$btnSalvarRegistroGastos = array(
	    'name' => 'btnAgregarGastos',
   		'class' => 'btnSalvarGastos',
	    'value' => 'Guardar'
	    );

echo form_input($cantidad);
echo form_textarea($descripcion);
echo form_button('btnCancelarRegistroGastos','Cancelar',"class='btnCancelarRegistroGastos'" );
echo form_submit($btnSalvarRegistroGastos);




echo form_close();

?>
</div>


<div class='cuadroModificarIngresos'>


    <p class='letrasModificarIngresos'>Modificar Ingresos</p>
	<hr>

<?php





echo form_open("index/modificarIngresosGuardados");

$cantidad = array(
	'name' => 'ModcantidadIngresos',
	'class' => 'txtCantidadRegistrar',
	'onkeypress' => 'return justNumbers(event);'
	);
$descripcion = array(
	'name' => 'ModdescripcionIngresos',
	'class'  => 'descripcionIngresos'
	);



$btnCancelarModificarIngresos = array(
	    'name' => 'btnCancelarModificarIngresos',
	    'value' => 'Cancelar'
	    );
$btnSalvarModificarIngresos = array(
	    'name' => 'btnSalvarModificarIngresos',
   		'class' => 'btnSarlvarModificaIngresos',
	    'value' => 'Actualizar'
	    );

echo form_input('ModcantidadIngresos',$mostrarTotalModIngresos['monto'],"class='fechaPersonal'");
echo form_input('ModdescripcionIngresos',$mostrarTotalModIngresos['descripcion'],"class='emailPersonal'");
echo form_button('btnCancelarModificarIngresos','Cancelar',"class='btnCancelarModificarIngresos'" );
echo form_submit($btnSalvarModificarIngresos);

echo form_close();
?>


</div>

<div class='cuadroModificarGastos'>


    <p class='letrasModificarGastos'>Modificar Gastos</p>
	<hr>

<?php

echo form_open("index/modificarGastosGuardados");

$btnCancelarModificarGastos = array(
	    'name' => 'btnCancelarModificarGastos',
	    'value' => 'Cancelar'
	    );
$btnSalvarModificarGastos = array(
	    'name' => 'btnSarlvarModificaGastos',
   		'class' => 'btnSarlvarModificaGastos',
	    'value' => 'Actualizar'
	    );


echo form_button('btnCancelarModificarGastos','Cancelar',"class='btnCancelarModificarGastos'" );
echo form_submit($btnSalvarModificarGastos);

echo form_close();
?>


</div>


<div class='cuadroTips'>

	<p class='letrasRecomendaciones'>Recomendaciones</p>
	<i id='cerrarRecomendaciones' class="fa fa-times-circle fa-2x"></i>
	<hr>
	<div class='contenedorLetras'>
<p class='ConsejosSemanales'>
Eso puede ahorrarte mucho dinero, el ser organizado trae muchas ventajas en la vida, una de 
ellas puede ser ahorrar dinero. Lo que hice para solucionar el problema que mencionaba antes,
fue dormir mas temprano y 	levantarme mas temprano, así hacia todas las cosas con calma,
desayunaba a gusto y llegaba temprano a clases.<p>
</div>

<p class='letrasOfertas'>Revisa ofertas, ¡Economiza!</p>
<hr>

<div class='cabeceraFrutas'>
<i class="fa fa-apple fa-5x"></i>
<p>Frutas y Vegetales</p>
</div>
<div class='Frutas'></div>


<div class='cabeceraCarne'>
<i class="fa fa-linux fa-5x"></i>
<p>Carne y Marisco</p>
</div>

<div class='Carne'></div>


<div class='cabeceraElectrodomestico'>
<i class="fa fa-laptop fa-5x"></i>
<p>Electrodomésticos</p>
</div>
<div class='Electrodomesticos'></div>


</div>


<div class='cuadroAhorro'>
	<p class='letrasAhorros'>Ahorros</p>
	<i  id='cerrarAhorros' class="fa fa-times-circle fa-2x"></i>
	<hr>

<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>


</div>


<div class='cuadroConfiguracion'>
	<p class='letrasConfiguracion'>Configuración</p>
	<i  id='cerrarConfiguracion' class="fa fa-times-circle fa-2x"></i>

	<hr>

<?php			 
		echo "<img src='../fotosUsuario/".$fotoPerfilDelusuario['foto']."' class='imagenUsuario2'/>";
?>

	 	<button class="BtnCambiarFoto" id="addImage"  onClick='return subirFotoPerfil()'>Cambiar Imagen</button>
	 
	 	 	<img  class="loaderAjax" id="loaderAjax" src="../img/default-loader.gif"/>
	 
	    	


<fieldset>
	<div id='datosPersonal'>
<?php

$idActualizarDatos = array('class' => 'actualizarDatos');

echo form_open("index/actualizarPassword",$idActualizarDatos);




$btnActualizarDatos = array(
    'name' => 'btnActualizarDatos',
	'value' => 'Actualizar Datos',
	'class' => 'btnActualizarDatos'
	 );
$btnCambiarPass = array(
    'name' => 'btnCambiarPass',
	'value' => 'Cambiar Contraseña',
	'class' => 'btnCambiarPass',
	'type' => 'button',
	'id' => 'cambiarContra'
	 );



	echo form_input('nombre',$datosPersonales['nombre'],"class='nombrePersonal'");
	echo form_input('apellido',$datosPersonales['apellido'],"class='ApellidoPersonal'");
	echo form_input('sexo',$datosPersonales['sexo'],"class='sexoPersonal'" );
	echo form_input('provincia',$datosPersonales['provincia'],"class='provinciaPersonal'");
	echo form_input('telefono',$datosPersonales['telefono'],"class='TelefonoPersonal'");
	echo form_input('fecha',$datosPersonales['fecha'],"class='fechaPersonal'");
	echo form_input('email',$datosPersonales['email'],"class='emailPersonal'");
	echo form_submit($btnActualizarDatos);
	echo form_submit($btnCambiarPass);
	


echo form_close();


?>


</div>
<div id='CuadroRecuperar' class='cuadroCambiasPass'>										

			
 	
<div id='actualizarPass'>

	<p id='cerrarActualizarDatos'><i  class="fa fa-times fa-2x"></i></p>
	<hr>

<?php


	$clasFormRecuperar = array('class' => 'classFormularioCambiarPass');
	echo form_open("index/ActualizarPassW", $clasFormRecuperar);

$passAntigua = array(
	'name'=>'antiguaPass',
	'class' => 'login-field',
	'type' => 'text',
	'id' => 'antiguaPass',
	'placeholder' => 'Escriba la antigua contraseña'
	
 	); 
$passNueva = array(
	'name'=>'nuevaPass',
	'class' => 'login-field',
	'type' => 'text',
	'id' => 'nuevaPass',
	'placeholder' => 'Escriba la contraseña nueva'
	
 	);
$repetirPassNueva = array(
	'name'=>'repitaNuevaPass',
	'class' => 'login-field',
	'type' => 'text',
	'id' => 'repitaNuevaPass',
	'placeholder' => 'Repita la contraseña nueva'
	
 	);
$btnRecuperar = array(
	'name'=>'CambiarPass',
	'class' => 'btnActualizarPass',
	'type' => 'submit',
	'value' => 'Actualizar contraseña',
	'id' => 'btnRecuperar'
 	);

		    echo form_input($passAntigua);
		    echo form_input($passNueva);
		    echo form_input($repetirPassNueva);
		    echo form_input($btnRecuperar);


	echo form_close();
?>
	</div>	

		
		</div>



</div>


</div>
<div class='mensajePrivado'>

		<a class='nuevoMensaje' >Enviar nuevo mensaje</a>
		<hr>








<div class='nuevoMensajeUsuario'>
		<p id='cerrarMensajeNuevo'><i  class="fa fa-times fa-2x"></i></p>
<?php

$idenviarMensajes = array('class' => 'enviarMensajes');

echo form_open("index/enviarMensaje",$idenviarMensajes);



    $usuarioMensaje = array(
		'name' =>  'nombreUsuario',
		'placeholder' => 'Nombre del usuario',
		'class' => 'txtUsuarioMensaje'
		);
	$mensajeUsuario = array(
	    'name' =>  'cuerpoMensaje',
		'class' => 'txtCuerpoMensaje',
	    'placeholder' => 'Cuerpo del Mensaje',
       	'rows'  => '5',
        'cols'  => '10'
		);
	$enviarMensaje = array(
		'name' =>  'btnEnviarMsj',
		'class' => 'btnEnviarMsj',
		'type' => 'submit',
		'value' => 'Enviar Mensaje'

		);

	echo form_input($usuarioMensaje);
	echo form_textarea($mensajeUsuario);
	echo form_input($enviarMensaje);


	echo form_close();
?>

</div>