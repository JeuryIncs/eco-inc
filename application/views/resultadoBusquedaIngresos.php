	

	<?php foreach ($ingresosGuardados as $datos ): ?>
    		
        <tr>
			<td>
				<?php echo $datos['monto'] ?>
			</td>
			<td>
				<?php echo $datos['descripcion'] ?>
			</td>
			<td>
				<?php echo $datos['fecha'] ?>
			</td>

		 	<td>
		 		<a  href="index/modificarIngresosGuardados?id=<?php echo $datos['id'] ?>">
		 			<p class='modificarGastos'>
		 				<i class='fa fa-pencil fa-2x'>
		 				</i>
					</p>
				</a>
				<a onClick='return eliminarIngresos()' href="index/eliminarIngresos?id=<?php echo $datos['id'] ?>">
					<p class='eliminarGasto'>
						<i class='fa fa-trash-o fa-2x'>
		 				</i>
		 			</p>
		 		</a>
		 	</td>
		</tr>	
    	<?php endforeach ?>