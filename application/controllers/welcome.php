<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Welcome extends CI_Controller {

function __construct(){
		parent::__construct();
		$this->load->helper('form');
		$this->load->model('ModeloRegistrar');
		$this->load->model('Inicio');		
   		$this->load->helper('url');
		$this->load->library('email');
		$this->load->library('session');
   		
	}

	public function index()
	{

		 if (isset($this->session->userdata['nombre']))
            {
              redirect('index', 'refresh');
            }
			
		$listaDeProvincias = $this->_listaDeProvincias();
		$this->load->view('header');
		$this->load->view('inicio', array('provincia' => $listaDeProvincias));
		$this->load->view('footer');
	}


	function validarUsuarioLogin(){
		

		$data = $this->ModeloRegistrar->validarUsuarios();

		if(empty($data)){
			echo "El usuario que intenta ingresar no existe";
		}else{	                 

			echo "bien";
			$datosLogin = array('nombre' => $data['nombre'],'id' =>$data['id'], 'usuario' =>$data['usuario']);
			$this->session->set_userdata($datosLogin); 
			
		}

	}


	private function _listaDeProvincias() {
			$provincias = json_decode(file_get_contents("Listaprovincias.txt"));
	    	return $provincias;
	}

	function recuperarCorreo(){

     $data = $this->ModeloRegistrar->validarCorreos();

      if(empty($data)){
			echo "No existe ese correo para recuperarlo";
			
		}else{

		     $correo = $data['email'];
			 $titulo = "Recuperar contraseña | EcoInc";			 
             $mensaje = ("Saludos. 

              Su contraseña es:   ".$data['clave']." 

              esperamos que no se le olvide nuevo, para facilidad
              cambiarla por una que se pueda recordar mas facil.

              Hasta Luego.");

            $this->email->from('ecoincs@gmail.com');
            $this->email->to($correo);
            $this->email->subject($titulo);               
            $this->email->message($mensaje); 

            if($this->email->send()) 
            {           
            echo  'La contraseña ha sido enviada a su email';      
           
            }else{
                echo  'No se pudo enviar la contraseña';
             
            } 
		
		}

	}


	
	function recibirDatatosRegistrar(){
	
	
       $data = $this->ModeloRegistrar->validarUsuarioRegistrar();


       if(empty($data)){

		echo "Felicides te has registrado con exito :)";
        $this->ModeloRegistrar->registrarUsuarioRegistrar();
       }else{


       	echo "Usuario o Correo ocupado por favor utilice otro";


       }
	   

	}


	
}

