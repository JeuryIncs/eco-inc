<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Index extends CI_Controller {

	function __construct(){
			parent::__construct();
			$this->load->helper('form');
			$this->load->model('Inicio');
		    $this->load->library('session');
		    $this->load->library('email');
            $this->load->helper('url'); 
            $this->load->library('table');
    }

	function index(){
        
            if (empty($this->session->userdata['nombre']))
            {
              redirect('../', 'refresh');
            }    
                       
        $emailLogeado = $this->session->userdata['nombre'];
        $ingresosGuardados = $this->Inicio->mostrarIngresosGuardados();
        $gastosGuardados = $this->Inicio->mostrarGastosGuardados();
        $totalGastoDinero = $this->Inicio->mostrarTotalGastos();
        $totalIngresosDinero = $this->Inicio->mostrarTotalIngresos();
        $datosPersonales = $this->Inicio->mostrarDatosPersonales();
        $fotoDelUsuario = $this->Inicio->mostarFotoPerfil();
        $graficaIngresos = $this->Inicio->mostrarGraficaUsuario();
        $mostrarTotalModIngresos = $this->Inicio->mostrarModIngresos();
        
		$this->load->view('headerIncio', array('emailLogeado' =>$emailLogeado,'fotoPerfilDelusuario' => $fotoDelUsuario));
		$this->load->view('bienvenido', array('ingresosGuardados' => $ingresosGuardados,'totalIngresosDinero' => $totalIngresosDinero,
         'gastosGuardados' =>$gastosGuardados, 'totalGastos' => $totalGastoDinero, 'datosPersonales' => $datosPersonales,
         'fotoPerfilDelusuario' => $fotoDelUsuario, 'mostrarGraficaIngresos' => $graficaIngresos
     ,'mostrarTotalModIngresos' => $mostrarTotalModIngresos));
           $this->load->view('footerInicio');


		
    }

    
 

function cambiarFoto(){

sleep(2);
$nombre = $this->session->userdata['nombre'];
$fotoUsario = $this->session->userdata['usuario'];


define("maxUpload", 50000);
define("maxWidth", 12000);
define("maxHeight", 12000);
define("uploadURL", 'fotosUsuario/');
define("fileName", $nombre.'_');


$tiposDeArchivo = array('image/jpeg','image/pjpeg','image/png');

    $pasaImgSize = false;
    $respuestaFile = false;
    $fileName = '';
    $mensajeFile = 'ERROR EN EL SCRIPT';

    $tamanio = $_FILES['userfile']['size'];
    $tipo = $_FILES['userfile']['type'];
    $archivo = $_FILES['userfile']['name'];

    $imageSize = getimagesize($_FILES['userfile']['tmp_name']);
                            
    $extension = explode('.',$_FILES['userfile']['name']);
    $num = count($extension)-1;

    $imgFile = $fotoUsario.'.'.$extension[$num];


        if(is_uploaded_file($_FILES['userfile']['tmp_name']))
        {
            if(move_uploaded_file($_FILES['userfile']['tmp_name'], uploadURL.$imgFile))
            {
                $respuestaFile = 'done';
                $fileName = $imgFile;
                $mensajeFile = $imgFile;
                $this->Inicio->actualizarFoto($fileName);

            }
            else
                $mensajeFile = 'No se pudo subir el archivo';
        }
        else
            $mensajeFile = 'No se pudo subir el archivo';
        
        

    $salidaJson = array("respuesta" => $respuestaFile,
                        "mensaje" => $mensajeFile,
                        "fileName" => $fileName);

    echo json_encode($salidaJson);


    }


    function enviarCorreo(){

    	    $nombre = $this->input->post('nombre');
            $correo = $this->input->post('correo');
            $titulo = $this->input->post('titulo');
            $mensaje = $this->input->post('mensaje');
                       
            $this->email->from($correo);
            $this->email->to('ecoincs@gmail.com');
            $this->email->subject($titulo);               
            $this->email->message($mensaje); 
           
            if($this->email->send())
            {           
            $data['title']='Mensaje Enviado';
            $data['msg'] = 'Mensaje enviado a su email';                            
            $this->load->view('bienvenido', $data); 
           
            }else{
                $data['title']='El mensaje no se pudo enviar';
             
            } 
        
    }

      function _tiempoActual(){
        date_default_timezone_set('Europe/Brussels');
        $fecha = date("d-m-Y G:i A");
        return  $fecha;
    }


    

 function agregarIngresos(){
 
    $nuevosIngresos = array(

            'cantidad' => $this->input->post('cantidadIngresos'), 
            'detalles' => $this->input->post('descripcionIngresos'), 
            'fecha' =>    $this->_tiempoActual(),
            'usuario' => $this->session->userdata['usuario']
    );

        $this->Inicio->guardarNuevosIngresos($nuevosIngresos);        
        $retornoIngresos['ingresosGuardados'] = $this->Inicio->busquedaIngresos();
        echo $this->load->view('resultadoBusquedaIngresos',$retornoIngresos,TRUE);
     
}

    function agregarGastos(){

           $nuevosGastos = array(

                    'cantidad' => $this->input->post('cantidadGastos'), 
                    'detalles' => $this->input->post('descripcionGastos'), 
                    'fecha' =>    $this->_tiempoActual(),
                    'usuario' =>  $this->session->userdata['usuario']
            );

                $this->Inicio->guardarNuevosGastos($nuevosGastos);
                $retorno['gastosGuardados'] = $this->Inicio->busquedaGasto();
                echo $this->load->view('resultadoBusquedaGasto',$retorno,TRUE);

    }

   function registrarUsuarioFB(){

        $usuariosFB = array(
            'fbId' => $this->input->post('fbid'), 
            'foto' => $this->input->post('foto'), 
            'nombre' => $this->input->post('nombre'), 
            'apellido' =>  $this->input->post('apellido'), 
            'sexo' =>  $this->input->post('sexo')

            );

        $this->Inicio->guardarDatosFacebook($usuariosFB);

    }

    function eliminarIngresos()
    {

        $id = $_GET['id'];
        $this->Inicio->eliminarIngresosGuardados($id);
        redirect('index');
    }

   function modificarIngresosGuardado(){

        $id = $_GET['id'];
        $this->Inicio->mostrarModIngresos($id);

   }


    function enviarMensaje(){

        $resultadoMensajes =  $this->Inicio->validarUsuarioMensajesPrivados();

        if(empty($resultadoMensajes)){
            
            echo "El usuario que intenta enviar mensaje no exite";
            
        }else{

            echo "Mensaje enviado!";
            $this->Inicio->enviarMensajesPrivados();
        }

    }

    function actualizarDatosPersonales(){

        $datosActualizar = array(
            'nombre' => $this->input->post('nombre'),
            'apellido' => $this->input->post('apellido'),
            'sexo' => $this->input->post('sexo'),
            'telefono' => $this->input->post('telefono'),
            'fecha' => $this->input->post('fecha'),
            'email' => $this->input->post('email'),
            'provincia' => $this->input->post('provincia')
            );


        $this->Inicio->actualizarDatos($datosActualizar);
        redirect('index');
    }

    function ActualizarPassW(){

       
     $data = $this->Inicio->validarActualizarPass();
     $datos = $this->Inicio->validarNewActualizarPass();

     if(empty($data)){

        echo "Por favor escriba su anterior contraseña";

         }else if(empty($datos)){

           echo "La contraseña ha sido cambiada con exito!";
                    $this->Inicio->ActualizarPass();

            }else{

              echo "Su nueva contraseña debe ser difente a la anterior";
                         
         }

    }

    function cerrarSession(){

    $this->session->unset_userdata('email');
    $this->session->sess_destroy();
    redirect('../','index');


    }

    function busquedaDeGasto(){
        $retorno['gastosGuardados'] = $this->Inicio->busquedaGasto();
        echo $this->load->view('resultadoBusquedaGasto',$retorno,TRUE);
    }

    function busquedaDeIngresos(){
        $retornoIngresos['ingresosGuardados'] = $this->Inicio->busquedaIngresos();
        echo $this->load->view('resultadoBusquedaIngresos',$retornoIngresos,TRUE);
    }

}