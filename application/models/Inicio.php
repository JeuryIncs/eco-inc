<?php

if(!defined('BASEPATH')) exit('No direct script access allowed');


class Inicio extends CI_Model
{
	
	function __construct(){

		parent::__construct();
		$this->load->database();		
		$this->load->library('session');
	}

	function busquedaGasto(){

		$ingresadoWEb = $this->input->post('buscadorGastos');

		$buqueda =  array(
			'descripcion' => $ingresadoWEb,
			'monto' => $ingresadoWEb,
			'fecha' => $ingresadoWEb
		);
		
		$this->db->or_like($buqueda);
		$query = $this->db->get('gastosUsuarios');
		return $query->result_array();
	}

		function busquedaIngresos(){

			$textoIngresos = $this->input->post('buscadorIngresos');

				$buqueda =  array(
					'descripcion' => $textoIngresos,
					'monto' => $textoIngresos,
					'fecha' => $textoIngresos
				);
				
				$this->db->or_like($buqueda);
				$query = $this->db->get('ingresosusuarios');
				return $query->result_array();
	}

	function validarUsuarioMensajesPrivados(){

		  $datosMensajePrivados = array(
            'usuario' => $this->input->post('nombreUsuario')
            );

		 $query = $this->db->get_where('usuariosEcoInc', $datosMensajePrivados);			
     
        return $query->row_array();



	}

	function enviarMensajesPrivados(){

        $usuarioConectado = $this->session->userdata['usuario'];

		$datosMensajePrivados = array(
            'nombreUsuarioMensaje' => $this->input->post('nombreUsuario'),
            'cuerpoMensaje' => $this->input->post('cuerpoMensaje'),
            'fecha' =>    $this->_horaActual(),   
            'usuario' =>  $usuarioConectado   
           );

           $this->db->insert('mensajes', array('usuariorecibe'=>$datosMensajePrivados['nombreUsuarioMensaje'], 'usuarioenvia'=>$datosMensajePrivados['usuario'],
		                    'mensaje' =>$datosMensajePrivados['cuerpoMensaje'], 'hora' =>$datosMensajePrivados['fecha']


       	));	


	}


	function guardarDatosFacebook($usuariosFB){

			$idFacebook = $usuariosFB['fbid'];

			$comprovarUsuarios = $this->db->query("select * from usuariosEcoInc where fbID ='".$idFacebook."'");
			
             if(empty($comprovarUsuarios->row_array)){
  

  			  $this->db->insert('usuariosEcoInc', array('fbID' => $usuariosFB['fbId'],'foto' => $usuariosFB['foto'],
  			   'nombre' => $usuariosFB['nombre'], 'apellido' => $usuariosFB['apellido'], 
  			   'sexo' => $usuariosFB['sexo']));

  			redirect('noexiste.php');

  	//aqui va para agregar 
  				}
             
             	else{
						redirect('Ya existe');
             		//retornar los valorsues del usuario
             	}


             }


	

	function guardarNuevosIngresos($nuevosIngresos){


		$this->db->insert('ingresosUsuarios', array('monto'=>$nuevosIngresos['cantidad'], 'descripcion'=>$nuevosIngresos['detalles'],
		                  'fecha' =>$nuevosIngresos['fecha'], 'usuario' =>$nuevosIngresos['usuario']));


	}

	function guardarNuevosGastos($nuevosGastos){


		$this->db->insert('gastosUsuarios', array('monto'=>$nuevosGastos['cantidad'], 'descripcion'=>$nuevosGastos['detalles'],
		                  'fecha' =>$nuevosGastos['fecha'], 'usuario' =>$nuevosGastos['usuario']));


	}
	function eliminarIngresosGuardados($id){

			$this->db->where('id',$id);
			$this->db->delete('ingresosUsuarios');
	}


	function eliminarGastosGuardados($id){

			$this->db->where('id',$id);
			$this->db->delete('gastosUsuarios');
	}




	function mostrarIngresosGuardados(){

		$ingresos = $this->db->get('ingresosUsuarios');        
        return $ingresos->result_array();
	}

	function mostrarGastosGuardados(){

		$gastos = $this->db->get('gastosUsuarios');        
        return $gastos->result_array();
	}


	function mostrarModIngresos($id = ""){

		if(isset($id)){

		$resulIngresos = $this->db->query("SELECT * FROM ingresosusuarios WHERE id = '$id'");  
        return $resulIngresos->row_array();


		}
	}


	function mostrarGraficaUsuario(){

        $usuarioConectado = $this->session->userdata['usuario'];
		$resultGrafica = $this->db->query("SELECT monto FROM ingresosusuarios WHERE usuario = '$usuarioConectado'");  
       
        return $resultGrafica->row_array();
	}

    function actualizarFoto($nombreFoto){


		    $idUsuario = $this->session->userdata['id'];
		    $actualizarFoto = $this->db->query("UPDATE usuariosEcoInc SET foto='$nombreFoto' where id='$idUsuario'");

		    return $actualizarFoto->result_array();
	}
	function mostarFotoPerfil(){


		    $idUsuario = $this->session->userdata['id'];
		    $mostarFoto = $this->db->query("SELECT * FROM usuariosecoinc where id='$idUsuario'");

		    return $mostarFoto->row_array();
	}

	function mostrarDatosPersonales(){

  
            $idLogeado = $this->session->userdata['id'];
		    $resultadoDatosPersonales = $this->db->query("SELECT * FROM usuariosecoinc WHERE id = '$idLogeado'");  
     		
        return $resultadoDatosPersonales->row_array();
	}

	function validarActualizarPass(){

		 $antiguaPass = $_POST['antiguaPass'];
         $usuarioConectado = $this->session->userdata['usuario'];

 		 $resultadoPass = $this->db->query("SELECT usuario,clave FROM usuariosecoinc WHERE usuario = '$usuarioConectado' and clave ='$antiguaPass'");  
     	 return $resultadoPass->row_array();

	}
	
	function validarNewActualizarPass(){

		 $nuevaPass = $_POST['nuevaPass'];
         $usuarioConectado = $this->session->userdata['usuario'];

 		 $resultadoPass = $this->db->query("SELECT usuario,clave FROM usuariosecoinc WHERE usuario = '$usuarioConectado' and clave ='$nuevaPass'");  
     	 return $resultadoPass->row_array();

	}

	function ActualizarPass(){

	 $nuevaPass = $_POST['nuevaPass'];

     $usuarioConectado = $this->session->userdata['usuario'];
     $this->db->query("UPDATE usuariosEcoInc SET clave='$nuevaPass' where usuario='$usuarioConectado'");  
     
	}

	function mostrarTotalIngresos(){

            $usuarioConectado = $this->session->userdata['usuario'];
		    $result = $this->db->query("SELECT SUM(monto) as total FROM ingresosusuarios WHERE usuario = '$usuarioConectado'");  
     
        return $result->result_array();
	}

	function mostrarTotalGastos(){

            $usuarioConectado = $this->session->userdata['usuario'];
		    $result = $this->db->query("SELECT SUM(monto) as total FROM gastosusuarios WHERE usuario = '$usuarioConectado'");  
     
        return $result->result_array();
	}

	

	function actualizarDatos($datosActualizar){
			

			$data = array('nombre' => $datosActualizar['nombre'], 'apellido' => $datosActualizar['apellido'],
			 'sexo' => $datosActualizar['sexo'],'telefono' => $datosActualizar['telefono'],
			 'fecha' => $datosActualizar['fecha'],'email' => $datosActualizar['email'],
			 'provincia' => $datosActualizar['provincia']);


		$idUsuario = $this->session->userdata['id'];
        $where = "id = '$idUsuario'"; 
		   
		$this->db->update('usuariosEcoInc', $data, $where);
		

	}

	protected function _horaActual(){

		$fecha=time();
		$horas = +2;
		$fecha += ($horas * 60 * 60);
		$fecha = date("H:i" , $fecha);
        return  $fecha;
    }

}

